package main

import "testing"

func TestXxx(t *testing.T) {
	firstResult := HelloWorldService()
	if firstResult != 1 {
		t.Errorf("Expected 1 for first execution, but go %d", firstResult)
	}

	secondResult := HelloWorldService()
	if secondResult != 2 {
		t.Errorf("Expected 2 for first execution, but go %d", secondResult)
	}
}
