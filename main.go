package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
)

var calls = 0

func HelloWorldService() int {
	calls++
	return calls
}

func HelloWorldController(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintf(w, "You have called me %d times.\n", HelloWorldService)
}

func loadEnv() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	loadEnv()
	var port = os.Getenv("PORT")

	if port == "" {
		port = ":80"
	}

	http.HandleFunc("/", HelloWorldController)
	_, err := fmt.Println(http.ListenAndServe(port, nil))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("ok")
}
